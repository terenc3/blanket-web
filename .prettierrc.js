/**
 * SPDX-FileCopyrightText: 2021 Benjamin Kahlau
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This file is part of Blanket Web.
 */
module.exports = {
    printWidth: 180,
    trailingComma: 'none',
    tabWidth: 4,
    semi: false,
    singleQuote: true
}
