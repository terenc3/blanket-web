# Blanket Web
> Web clone of [Blanket](https://github.com/rafaelmardojai/blanket) by Rafael Mardojai CM and contributors

## Development
* `npm i`
* `npm run webpack`

## Screenshot
![Screenshot](./screenshot.png)
