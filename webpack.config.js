/**
 * SPDX-FileCopyrightText: 2021 Benjamin Kahlau
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This file is part of Blanket Web.
 */
const path = require('path')

const FaviconsWebpackPlugin = require('favicons-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    mode: 'development',
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'index.js',
        publicPath: './',
    },
    devServer: {
        static: {
            directory: path.join(__dirname, 'dist'),
        }
    },
    plugins: [
        new FaviconsWebpackPlugin({
            logo: './src/index.svg',
            favicons: {
                appName: 'Blanket',
                display: 'fullscreen',
                orientation: 'portrait',
                start_url: 'https://roanapur.de/blanket/'
            }
        }),
        new HtmlWebpackPlugin({  // Also generate a test.html
            template: 'src/index.html'
        })
    ],
    module: {
        rules: [
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader', 'postcss-loader'],
            },
            {
                test: /\.(ogg|svg)$/i,
                type: 'asset/resource'
            },
        ],
    },
}
