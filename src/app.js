/**
 * SPDX-FileCopyrightText: 2021 Benjamin Kahlau
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This file is part of Blanket Web.
 */
const Sound = require('./sound')

module.exports = {
    data: () => {
        return {
            data: require('./sounds')
        }
    },
    components: {
        Sound
    }
}
