/**
 * SPDX-FileCopyrightText: 2021 Benjamin Kahlau
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This file is part of Blanket Web.
 */
const CreativeCommonsLicenses = {
    'CC-BY-4.0': {
        name: 'Creative Commons Attribution 4.0 International',
        url: 'http://creativecommons.org/licenses/by/4.0/'
    },
    'CC-BY-SA-4.0': {
        name: 'Creative Commons Attribution Share Alike 4.0 International',
        url: 'http://creativecommons.org/licenses/by-sa/4.0/'
    },
    'CC-BY-3.0': {
        name: 'Creative Commons Attribution 3.0 Unported',
        url: 'https://creativecommons.org/licenses/by/3.0/'
    },
    'CC-BY-SA-3.0': {
        name: 'Creative Commons Attribution Share Alike 3.0 Unported',
        url: 'https://creativecommons.org/licenses/by-sa/3.0/'
    },
    'CC0-1.0': {
        name: 'Creative Commons Zero v1.0 Universal',
        url: 'https://creativecommons.org/publicdomain/zero/1.0/'
    },
    'CC-SP-1.0': {
        name: 'Creative Commons Sampling Plus 1.0',
        url: 'https://creativecommons.org/licenses/sampling+/1.0/'
    }
}

module.exports = {
    template: `<span class="text-xs text-neutral-700 float-right"><a v-bind:href="url" class="underline">{{ title }}</a> by {{ author }} is <span v-if="license !== 'Public Domain'">licensed under a <a v-bind:href="licenseUrl" class="underline">{{ licenseName }} License</a></span><span v-if="license === 'Public Domain'">Public Domain</span></span>`,
    props: {
        title: String,
        author: String,
        license: String,
        url: String
    },
    computed: {
        licenseUrl: function () {
            return CreativeCommonsLicenses[this.license]?.url
        },
        licenseName: function () {
            return CreativeCommonsLicenses[this.license]?.name
        }
    }
}
