/**
 * SPDX-FileCopyrightText: 2021 Benjamin Kahlau
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This file is part of Blanket Web.
 */
require('./index.css')

const Vue = require('vue')

const App = require('./app')

const app = Vue.createApp(App)
app.mount('#app')
