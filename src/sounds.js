/**
 * SPDX-FileCopyrightText: 2021 Benjamin Kahlau
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This file is part of Blanket Web.
 */
module.exports = [
    {
        title: 'Nature',
        sounds: [
            {
                title: 'Rain',
                file: require('./assets/sounds/rain.ogg'),
                icon: require('./assets/icons/com.rafaelmardojai.Blanket-rain-symbolic.svg'),

                name: ' rain ambience',
                author: 'alex36917',
                license: 'CC-BY-3.0',
                url: 'https://freesound.org/people/alex36917/sounds/524605/'
            },
            {
                title: 'Storm',
                file: require('./assets/sounds/storm.ogg'),
                icon: require('./assets/icons/com.rafaelmardojai.Blanket-storm-symbolic.svg'),

                name: ' Infinite Storm.wav',
                author: 'digifishmusic',
                license: 'CC-BY-3.0',
                url: 'https://freesound.org/people/digifishmusic/sounds/41739/'
            },
            {
                title: 'Wind',
                file: require('./assets/sounds/wind.ogg'),
                icon: require('./assets/icons/com.rafaelmardojai.Blanket-wind-symbolic.svg'),

                name: 'USA, Texas » Wind blowing in a field in Texas, USA',
                author: 'felix.blume',
                license: 'CC0-1.0',
                url: 'https://freesound.org/people/felix.blume/sounds/217506/'
            },
            {
                title: 'Waves',
                file: require('./assets/sounds/waves.ogg'),
                icon: require('./assets/icons/com.rafaelmardojai.Blanket-waves-symbolic.svg'),

                name: ' Ambient Nature Soundscapes » oceanwavescrushing.wav',
                author: 'Luftrum',
                license: 'CC-BY-3.0',
                url: 'https://freesound.org/people/Luftrum/sounds/48412/'
            },
            {
                title: 'Stream',
                file: require('./assets/sounds/stream.ogg'),
                icon: require('./assets/icons/com.rafaelmardojai.Blanket-stream-symbolic.svg'),

                name: 'stream » stream2.wav',
                author: 'gluckose',
                license: 'CC0-1.0',
                url: 'https://freesound.org/people/gluckose/sounds/333987/'
            },
            {
                name: ' WoodThrushinMorningShawneeForestMay272012.wav',
                title: 'Birds',
                file: require('./assets/sounds/birds.ogg'),
                icon: require('./assets/icons/com.rafaelmardojai.Blanket-birds-symbolic.svg'),

                author: 'kvgarlic',
                license: 'CC0-1.0',
                url: 'https://freesound.org/people/kvgarlic/sounds/156826/'
            },
            {
                title: 'Summer Night',
                file: require('./assets/sounds/summer-night.ogg'),
                icon: require('./assets/icons/com.rafaelmardojai.Blanket-summer-night-symbolic.svg'),

                name: 'Crickets Chirping At Night',
                author: 'Lisa Redfern',
                license: 'Public Domain',
                url: 'https://soundbible.com/2083-Crickets-Chirping-At-Night.html'
            }
        ]
    },
    {
        title: 'Travel',
        sounds: [
            {
                title: 'Train',
                file: require('./assets/sounds/train.ogg'),
                icon: require('./assets/icons/com.rafaelmardojai.Blanket-train-symbolic.svg'),

                name: 'Rainy Train',
                author: 'sylvanhomestead',
                license: 'CC-SP-1.0',
                url: 'https://trains.ambient-mixer.com/rainy-train'
            },
            {
                title: 'Boat',
                file: require('./assets/sounds/boat.ogg'),
                icon: require('./assets/icons/com.rafaelmardojai.Blanket-boat-symbolic.svg'),

                name: 'Montenegro » Ambience_Sea_Boat_Night_Ropes_4.wav',
                author: 'Falcet',
                license: 'CC0-1.0',
                url: 'https://freesound.org/people/Falcet/sounds/439365/'
            },
            {
                title: 'City',
                file: require('./assets/sounds/city.ogg'),
                icon: require('./assets/icons/com.rafaelmardojai.Blanket-city-symbolic.svg'),

                name: 'City noise » NYC_street leve02l.wav',
                author: 'gezortenplotz',
                license: 'CC-BY-3.0',
                url: 'https://freesound.org/people/gezortenplotz/sounds/44796/'
            }
        ]
    },
    {
        title: 'Interiors',
        sounds: [
            {
                title: 'Coffee Shop',
                file: require('./assets/sounds/coffee-shop.ogg'),
                icon: require('./assets/icons/com.rafaelmardojai.Blanket-coffee-shop-symbolic.svg'),

                name: 'Restaurant Ambiance',
                author: 'stephan',
                license: 'Public Domain',
                url: 'https://soundbible.com/1664-Restaurant-Ambiance.html'
            },
            {
                title: 'Fireplace',
                file: require('./assets/sounds/fireplace.ogg'),
                icon: require('./assets/icons/com.rafaelmardojai.Blanket-fireplace-symbolic.svg'),

                name: 'Fireplace',
                author: 'ezwa',
                license: 'Public Domain',
                url: 'https://soundbible.com/1543-Fireplace.html'
            }
        ]
    },
    {
        title: 'Noise',
        sounds: [
            {
                title: 'Pink Noise',
                file: require('./assets/sounds/pink-noise.ogg'),
                icon: require('./assets/icons/com.rafaelmardojai.Blanket-pink-noise-symbolic.svg'),

                name: 'Pink noise.ogg',
                author: 'Omegatron',
                license: 'CC-BY-SA-3.0',
                url: 'https://es.wikipedia.org/wiki/Archivo:Pink_noise.ogg'
            },
            {
                title: 'White Noise',
                file: require('./assets/sounds/white-noise.ogg'),
                icon: require('./assets/icons/com.rafaelmardojai.Blanket-white-noise-symbolic.svg'),

                name: 'White-noise-sound-20sec-mono-44100Hz.ogg',
                author: 'Jorge Stolfi',
                license: 'CC-BY-3.0',
                url: 'https://commons.wikimedia.org/w/index.php?title=File%3AWhite-noise-sound-20sec-mono-44100Hz.ogg'
            }
        ]
    }
]
