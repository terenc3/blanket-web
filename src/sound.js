/**
 * SPDX-FileCopyrightText: 2021 Benjamin Kahlau
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This file is part of Blanket Web.
 */
const License = require('./license')

module.exports = {
    template: `<div class="flex">
                <img class="row-span-2 h-16 w-16 mr-2" v-bind:src="icon" v-on:click="onMute()" v-bind:class="{ 'opacity-25': disabled } " v-bind:alt="title + ' icon'"/>
                <div class="w-full">
                    <h3 class="text-xl">{{ title }} <License v-bind:title="name" v-bind:author="author" v-bind:license="license"  v-bind:url="url"></License></h3>
                    <audio ref="player" v-bind:src="file" autoplay controls loop @volumechange="onVolumeChange($event)" muted class="hidden"></audio>
                    <input class="w-full" type="range" min="1" max="100" v-model.number="volume" v-on:change="onChangeVolume" v-bind:disabled="disabled" v-bind:title="volume" @wheel.prevent="onWheel($event)"/>
                </div>
            </div>`,
    components: {
        License
    },
    data: () => {
        return {
            volume: 100,
            disabled: true
        }
    },
    props: {
        title: String,
        file: String,
        icon: String,

        name: String,
        author: String,
        license: String,
        url: String
    },
    methods: {
        onMute: function () {
            this.$refs.player.muted = !this.$refs.player.muted
            this.disabled = this.$refs.player.muted
            window.localStorage[this.file + '|muted'] = this.$refs.player.muted
            this.$refs.player.play()
        },
        onVolumeChange: function () {
            this.volume = Math.round(this.$refs.player.volume * 100)
            if (this.$refs.player.muted) {
                this.disabled = true
            }
            this.$refs.player.play()
        },
        onChangeVolume: function () {
            this.$refs.player.volume = Math.round(this.volume) / 100
            window.localStorage[this.file + '|volume'] = this.$refs.player.volume
        },
        onWheel: function (event) {
            this.$refs.player.volume = this.$refs.player.volume + event.deltaY * -0.0001
        }
    },
    mounted() {
        if (window.localStorage[this.file + '|muted']) {
            this.$refs.player.muted = window.localStorage[this.file + '|muted'] === 'true'
            this.disabled = this.$refs.player.muted
        }
        if (window.localStorage[this.file + '|volume']) {
            this.$refs.player.volume = parseFloat(window.localStorage[this.file + '|volume'])
            this.volume = this.$refs.player.volume
        }
    }
}
