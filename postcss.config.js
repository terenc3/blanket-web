/**
 * SPDX-FileCopyrightText: 2021 Benjamin Kahlau
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This file is part of Blanket Web.
 */
module.exports = {
    map: {
        inline: false
    },
    plugins: [
        require('postcss-import'),
        require('tailwindcss'),
        require('autoprefixer'),
        require('cssnano')({
            preset: 'default'
        })
    ]
}
