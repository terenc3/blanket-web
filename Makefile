# SPDX-FileCopyrightText: 2023 Benjamin Kahlau
#
# SPDX-License-Identifier: GPL-3.0-or-later
NODE := node
NPM := npm
INSTALL := /usr/bin/install

NAME := blanket-web
VERSION := $(shell git describe --abbrev=0 --tags 2> /dev/null || echo '1.0.0')

BIN := ./node_modules/.bin
PKG := $(BIN)/pkg
PRETTIER := $(BIN)/prettier
PRETTIER_FLAGS := --cache --cache-strategy metadata --write
ESLINT := $(BIN)/eslint
ESLINT_FLAGS := --fix --ignore-path .gitignore
WEBPACK := $(BIN)/webpack
WEBPACK_FLAGS := --mode development

SRC_JS := $(wildcard src/*.js)
SRC_MD := $(wildcard **/*.md)
DIST_FILES := $(wildcard dist/*)

all: node_modules format lint dist

pack: $(NAME)-$(VERSION)-dist.tgz ## Create browser archive
	@echo $(NAME)-$(VERSION)

preview: dist/preview.png dist/favicon.ico

node_modules: package.json
	$(NPM) prune && $(NPM) install

format: $(SRC_JS) $(SRC_MD) ## Format code with prettier
	$(PRETTIER) $^  $(PRETTIER_FLAGS)

lint: $(SRC_JS) ## Lint code with eslint
	$(ESLINT) $^ $(ESLINT_FLAGS)

dist:
	$(WEBPACK) $(WEBPACK_FLAGS)

dist/preview.png: preview.png
	$(INSTALL) -d $(dir $@)
	$(INSTALL) $^ $@

dist/favicon.ico: src/index.svg
	$(INSTALL) -d $(dir $@)
	convert -density 256x256 -background transparent $^ -define icon:auto-resize -colors 256 $@

$(NAME)-$(VERSION)-dist.tgz: $(DIST_FILES)
	tar -C dist/ -zcvf $@ $(patsubst dist/%,%,$^)

clean: ## Clean dist folder
	$(RM) -r dist/

help: ## Show this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-10s\033[0m %s\n", $$1, $$2}'

.PHONY: help format lint preview