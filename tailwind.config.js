/**
 * SPDX-FileCopyrightText: 2021 Benjamin Kahlau
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This file is part of Blanket Web.
 */
module.exports = {
    content: ['./src/**/*.{html,js}'],
    theme: {
        extend: {}
    },
    variants: {
        extend: {}
    },
    plugins: []
}
